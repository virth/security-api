﻿using System;
using System.IO;
using Pdftools.Pdf;
using Pdftools.PdfSecure;

namespace test_signature
{
    class Program
    {
        static void Main(string[] args)
        {
            var documentContent = File.ReadAllBytes("./test.pdf");

            var signedDocument = Program.SignDocument(documentContent);

            File.WriteAllBytes("./signed.pdf", signedDocument);
        }

        private static byte[] SignDocument(byte[] documentContent)
        {
            using (var doc = new Secure())
            {
                if (!doc.OpenMem(documentContent, string.Empty))
                {
                    throw new Exception(
                        "Error while opening PDF-Stream. " +
                        $"{doc.ErrorMessage} (PDFTools Security API ErrorCode: 0x{doc.ErrorCode:x}).");
                }

                doc.AddTimeStampSignature(new Signature {TimeStampURL = "http://tsa.swisssign.net"});


                if (!doc.SaveInMemory(string.Empty, string.Empty, PDFPermission.ePermNoEncryption, 0, string.Empty,
                    string.Empty))
                {
                    throw new Exception(
                        "Error while adding signature. " +
                        $"{doc.ErrorMessage} (PDFTools Security API ErrorCode: 0x{doc.ErrorCode:x}).");
                }

                doc.Close();

                var signedDocument = doc.GetPdf();
                if (signedDocument == null)
                {
                    throw new Exception("No output file received.");
                }

                doc.EndSession();
            }

            return documentContent;
        }
    }
}
